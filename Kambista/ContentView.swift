//
//  ContentView.swift
//  Kambista
//
//  Created by Rommy Fuentes Davila Otani on 13/04/21.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, world!")
            .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
