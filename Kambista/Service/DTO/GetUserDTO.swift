//
//  GetUserDTO.swift
//  Kambista
//
//  Created by Rommy Fuentes Davila Otani on 14/04/21.
//

import Foundation

struct GetUserDTO: Codable {
    
    var success: Bool = false
    var message: String = ""
}
