//
//  LogoImg.swift
//  Kambista
//
//  Created by Rommy Fuentes Davila Otani on 13/04/21.
//

import SwiftUI

struct LogoImg: View {
    var body: some View {
        Image("logo")
            .resizable()
            .scaledToFit()
            .frame(width: 200, height: 100, alignment: .center)
            .padding(.all, 50)
    }
}

struct LogoImg_Previews: PreviewProvider {
    static var previews: some View {
        LogoImg()
    }
}
