//
//  HomeView.swift
//  Kambista
//
//  Created by Rommy Fuentes Davila Otani on 13/04/21.
//

import SwiftUI

struct HomeView: View {
    fileprivate func WelcomeView() -> some View {
        return VStack(spacing: 20){
            VStack{
                Text("!Bienvenido!")
                    .font(Font.body.bold())
                Text("Correo")
                    .underline()
                    .padding(5)
            }
            
        }
        .padding(50)
        .cornerRadius(30)
        .background(RoundedRectangle(cornerRadius: 5).fill(Color.white).shadow(radius: 2))
    }
    
    var body: some View {
        ZStack{
            Color("background")
            VStack{
                LogoImg()
                WelcomeView()
                
            }
            .frame(minWidth: CGFloat(LOGIN_CONTAINER_WIDTH), maxWidth: CGFloat(LOGIN_CONTAINER_WIDTH), alignment: .center)
            
        }.ignoresSafeArea()
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
