//
//  LoginView.swift
//  Kambista
//
//  Created by Rommy Fuentes Davila Otani on 13/04/21.
//

import SwiftUI

struct LoginView: View {
    
    @State var username : String = ""
    @State var password : String = ""
    @State var isDisabled : Bool = true
    
    @ObservedObject var viewModel = LoginViewModel()
    
    fileprivate func UserTextField() -> some View {
        return TextField("lbl_user", text: $username)
            .onChange(of: username, perform: { value in
                isDisabled = !isValidForm()
            })
            .textFieldStyle(RoundedBorderTextFieldStyle())
            .keyboardType(.emailAddress)
    }
    
    fileprivate func PasswordTextField() -> some View {
        return SecureField("lbl_password", text: $password)
            .onChange(of: password, perform: { value in
                isDisabled = !isValidForm()
            })
            .textFieldStyle(RoundedBorderTextFieldStyle())
    }
    
    fileprivate func LoginButton() -> some View {
        return Button(action: {
            validLoginWS()
        }, label: {
            Text("btn_login")
        })
        .buttonStyle(LoginButtonStyle(isDisabled: isDisabled))
        .fullScreenCover(isPresented: $viewModel.success, content: {
            HomeView()
        })
    }
    
    var body: some View {
        ZStack{
            Color("background")
            VStack{
                LogoImg()
                VStack(spacing: 20){
                    UserTextField()
                    PasswordTextField()
                    LoginButton()
                }
            }
            .frame(minWidth: CGFloat(LOGIN_CONTAINER_WIDTH), maxWidth: CGFloat(LOGIN_CONTAINER_WIDTH), alignment: .center)
            
        }.ignoresSafeArea()
    }
    
    func validLoginWS(){
        viewModel.login(user: username, password: password)
    }
    
    func isValidForm() -> Bool{
        (username.count != 0 && password.count != 0)
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        Group{
            LoginView()
                .environment(\.colorScheme, .dark)
            LoginView()
                .environment(\.colorScheme, .light)
            LoginView()
                .previewDevice(PreviewDevice(rawValue: "iPhone 12 Pro Max"))
                .environment(\.colorScheme, .dark)
            LoginView()
                .previewDevice(PreviewDevice(rawValue: "iPhone 12 Pro Max"))
                .environment(\.colorScheme, .light)
    
        }
    }
}
