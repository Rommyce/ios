//
//  LoginButtonStyle.swift
//  Colegio
//
//  Created by Rommy Fuentes Davila Otani on 30/03/21.
//

import SwiftUI

struct LoginButtonStyle: ButtonStyle {
    
    @State var isDisabled : Bool
    
    func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .frame(minWidth: 0, maxWidth: .infinity)
            .padding()
            .foregroundColor(configuration.isPressed ? .gray : .black)
            .background(isDisabled ? Color.gray : Color("splash"))
            .cornerRadius(CGFloat(RADIUS_STAND))
            .font(Font.body.bold())
            .disabled(isDisabled)
    }
}
